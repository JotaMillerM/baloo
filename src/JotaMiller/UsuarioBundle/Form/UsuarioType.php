<?php

namespace JotaMiller\UsuarioBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UsuarioType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username')
            ->add('email')
            ->add('foto', 'file', array(
                    'data_class' => null,
                ))
            ->add('telefono')
            ->add('biografia')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'JotaMiller\UsuarioBundle\Entity\Usuario'
        ));
    }

    public function getName()
    {
        return 'jotamiller_usuariobundle_usuariotype';
    }
}
