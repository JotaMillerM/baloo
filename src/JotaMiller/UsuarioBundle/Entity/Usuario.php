<?php

namespace JotaMiller\UsuarioBundle\Entity;

use FOS\UserBundle\Entity\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Usuario
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Usuario extends BaseUser
{
    /**
     * @ORM\OneToMany(targetEntity="JotaMiller\BalooBundle\Entity\Comentario", mappedBy="usuario")
     */
    protected $comentarios;

    /**
     * @ORM\OneToMany(targetEntity="JotaMiller\FormBundle\Entity\Informe", mappedBy="usuario")
     */
    protected $informes;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var $foto
     * 
     * @ORM\Column(name="foto", type="text")
     */
    private $foto;

    /**
     * @var $telefono
     * 
     * @ORM\Column(name="telefono", type="string", length=10)
     */
    private $telefono;

    /**
     * @var $biografia
     * 
     * @ORM\Column(name="biografia", type="text")
     */
    private $biografia;


    public function __construct()
    {
        parent::__construct();
        $this->comentarios  = new ArrayCollection();
        $this->informes     = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add comentarios
     *
     * @param \JotaMiller\BalooBundle\Entity\Comentario $comentarios
     * @return Usuario
     */
    public function addComentario(\JotaMiller\BalooBundle\Entity\Comentario $comentarios)
    {
        $this->comentarios[] = $comentarios;
    
        return $this;
    }

    /**
     * Remove comentarios
     *
     * @param \JotaMiller\BalooBundle\Entity\Comentario $comentarios
     */
    public function removeComentario(\JotaMiller\BalooBundle\Entity\Comentario $comentarios)
    {
        $this->comentarios->removeElement($comentarios);
    }

    /**
     * Get comentarios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getComentarios()
    {
        return $this->comentarios;
    }

    /**
     * Add informes
     *
     * @param \JotaMiller\FormBundle\Entity\Informe $informes
     * @return Usuario
     */
    public function addInforme(\JotaMiller\FormBundle\Entity\Informe $informes)
    {
        $this->informes[] = $informes;
    
        return $this;
    }

    /**
     * Remove informes
     *
     * @param \JotaMiller\FormBundle\Entity\Informe $informes
     */
    public function removeInforme(\JotaMiller\FormBundle\Entity\Informe $informes)
    {
        $this->informes->removeElement($informes);
    }

    /**
     * Get informes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getInformes()
    {
        return $this->informes;
    }

    /**
     * Set foto
     *
     * @param string $foto
     * @return Alumno
     */
    public function setFoto($foto)
    {
        $this->foto = $foto;

        return $this;
    }

    /**
     * Get foto
     *
     * @return string 
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * Devuelve el path web
     */
    public function getWebPath()
    {
        return null === $this->foto
            ? null
            : $this->getUploadDir().'/'.$this->foto;
    }

    protected function getUploadDir()
    {
        return 'uploads/imagenes';
    }

    /**
     * Ruta absoluta en la cual se guardaran las imagenes
     */
    protected function getUploadRootDir()
    {
        // la ruta absoluta del directorio donde se deben
        // guardar los archivos cargados
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->path;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     * @return Usuario
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string 
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set biografia
     *
     * @param string $biografia
     * @return Usuario
     */
    public function setBiografia($biografia)
    {
        $this->biografia = $biografia;

        return $this;
    }

    /**
     * Get biografia
     *
     * @return string 
     */
    public function getBiografia()
    {
        return $this->biografia;
    }

}
