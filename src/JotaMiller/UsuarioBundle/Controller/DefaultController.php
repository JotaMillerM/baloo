<?php

namespace JotaMiller\UsuarioBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('JotaMillerUsuarioBundle:Default:index.html.twig', array('name' => $name));
    }
}
