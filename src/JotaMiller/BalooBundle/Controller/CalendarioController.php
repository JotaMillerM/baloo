<?php

namespace JotaMiller\BalooBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class CalendarioController extends Controller
{
	protected function includegoogleLib()
    {
    	$path = $this->container->getParameter( 'kernel.root_dir' );
      	require_once( $path . '/../src/google-api-php-client/src/Google_Client.php' );
      	require_once( $path . '/../src/google-api-php-client/src/contrib/Google_CalendarService.php' );
    }
    public function indexAction()
    {
    	$estado 	= 1;
    	$authUrl 	= "";

    	if (!isset($_SESSION['token'])) {
			$estado = 0;

	    	$this->includegoogleLib();
	        
			$client = new \Google_Client();
			$client->setApplicationName("baloo");

			$service = new \Google_CalendarService($client);
			$service = new \Google_CalendarService($client);
			if (isset($_GET['logout'])) {
			  	unset($_SESSION['token']);
			}

			if (isset($_GET['code'])) {
			  	$client->authenticate($_GET['code']);
			  	$_SESSION['token'] = $client->getAccessToken();
			  	header('Location: http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
			}

			if (isset($_SESSION['token'])) {
			 	$client->setAccessToken($_SESSION['token']);
			}
			
			if (!$client->getAccessToken()) {
				$authUrl = $client->createAuthUrl();
			}
			
    	}
        return $this->render('JotaMillerBalooBundle:Calendario:index.html.twig', array(
        	"conectado_google" => $estado,
        	"url_conexion" => $authUrl
        	)
        );
    }

    /**
     * Devuelve las fechas de los eventos en formato JSON
     * Se utiliza con fullcalendar
     */
    public function getEventosAction()
    {
    	$this->includegoogleLib();
        
		$client = new \Google_Client();
		$client->setApplicationName("baloo");

		$service = new \Google_CalendarService($client);
		if (isset($_GET['logout'])) {
		  unset($_SESSION['token']);
		}

		if (isset($_GET['code'])) {
		  $client->authenticate($_GET['code']);
		  $_SESSION['token'] = $client->getAccessToken();
		  header('Location: http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
		}

		if (isset($_SESSION['token'])) {
		  $client->setAccessToken($_SESSION['token']);
		}

		if ($client->getAccessToken()) {
		  	// $calList = $cal->calendarList->listCalendarList();
		  	$calList = $service->events->listEvents('primary');
		  	// print "<h1>Calendar List</h1><pre>" . print_r($calList, true) . "</pre>";

			$_SESSION['token'] = $client->getAccessToken();
		} else {
		  $authUrl = $client->createAuthUrl();
		  print "<a class='login' href='$authUrl'>Connect Me!</a>";
		}

		$array_eventos 	=	array();
		foreach ($calList['items'] as $evento) {

			// Inicio
			if (isset($evento['start']['date'])) {
				$fecha_inicio = $evento['start']['date'];
			}else{
				$fecha = new \DateTime($evento['start']['dateTime']);
				$fecha_inicio =  $fecha->format('Y-m-d H:i:s');
			}
			// Termino
			if (isset($evento['start']['date'])) {
				$fecha_termino = $evento['end']['date'];
			}else{
				$fecha = new \DateTime($evento['end']['dateTime']);
				$fecha_termino = $fecha->format('Y-m-d H:i:s');
			}
			$array_eventos[] = array(
				'title' => $evento['summary'],
				'start' => $fecha_inicio,
				'end' => $fecha_termino
				);
		}

		return new JsonResponse($array_eventos);
    }
}
