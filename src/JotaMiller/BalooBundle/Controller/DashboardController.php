<?php

namespace JotaMiller\BalooBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DashboardController extends Controller
{

    public function indexAction()
    {	  
        return $this->render('JotaMillerBalooBundle:Dashboard:index.html.twig', array());
    }
}
