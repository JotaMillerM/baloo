<?php

namespace JotaMiller\BalooBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

use JotaMiller\BalooBundle\Entity\Comentario;
use JotaMiller\BalooBundle\Entity\Alumno;
use JotaMiller\BalooBundle\Form\ComentarioType;

/**
 * Comentario controller.
 *
 */
class ComentarioController extends Controller
{
    /**
     * Lists all Comentario entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('JotaMillerBalooBundle:Comentario')->findAll();

        return $this->render('JotaMillerBalooBundle:Comentario:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Registra un nuevo comentario via Ajax
     */
    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $entity     =   new Comentario();
        $user       =   $this->getUser();
        $resp       =   array();

        $comentario = "vacio";
        if ($request->getMethod() == 'POST') {
            $comentario         =   $request->get('comentario');
            $id_alumno          =   $request->get('alumno');
            $alumno     =   $em->getRepository('JotaMillerBalooBundle:Alumno')->find($id_alumno);

            $entity->setUsuario( $user );
            $entity->setAlumno( $alumno );
            $entity->setFechaRegistro( new \DateTime());
            $entity->setComentario( $comentario );
            $entity->setEstado(1);

            $em->persist($entity);
            $em->flush();


            $ultimo_comentario = $em->getRepository('JotaMillerBalooBundle:Comentario')->find(  $entity->getId() );

            $resp = array(
                'fecha' => $ultimo_comentario->getFechaRegistro(),
                'autor' => $ultimo_comentario->getUsuario()->getUsername(),
                'comentario' => $ultimo_comentario->getComentario(),
                );
        }
        return new JsonResponse($resp);
    }

    /**
     * Creates a new Comentario entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new Comentario();
        $form = $this->createForm(new ComentarioType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('comentario_show', array('id' => $entity->getId())));
        }

        return $this->render('JotaMillerBalooBundle:Comentario:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new Comentario entity.
     *
     */
    public function newAction()
    {
        $entity = new Comentario();
        $form   = $this->createForm(new ComentarioType(), $entity);

        return $this->render('JotaMillerBalooBundle:Comentario:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Comentario entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JotaMillerBalooBundle:Comentario')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Comentario entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JotaMillerBalooBundle:Comentario:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Comentario entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JotaMillerBalooBundle:Comentario')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Comentario entity.');
        }

        $editForm = $this->createForm(new ComentarioType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JotaMillerBalooBundle:Comentario:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Comentario entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JotaMillerBalooBundle:Comentario')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Comentario entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new ComentarioType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('comentario_edit', array('id' => $id)));
        }

        return $this->render('JotaMillerBalooBundle:Comentario:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Comentario entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('JotaMillerBalooBundle:Comentario')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Comentario entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('comentario'));
    }

    /**
     * Creates a form to delete a Comentario entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
