<?php

namespace JotaMiller\BalooBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use JotaMiller\BalooBundle\Entity\Alumno;
use JotaMiller\BalooBundle\Entity\Comentario;
use JotaMiller\BalooBundle\Form\AlumnoType;

/**
 * Alumno controller.
 *
 */
class AlumnoController extends Controller
{
    /**
     * Lists all Alumno entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('JotaMillerBalooBundle:Alumno')->findAll();

        return $this->render('JotaMillerBalooBundle:Alumno:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Creates a new Alumno entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new Alumno();
        $form = $this->createForm(new AlumnoType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            
            $ext = $form['foto']->getData()->guessExtension();
            $filename = sha1(uniqid(mt_rand(), true)) .'.'. $ext;
            $form['foto']->getData()->move('uploads/imagenes', $filename);
           
            $em = $this->getDoctrine()->getManager();
            
            $entity->setFechaRegistro( new \DateTime() );
            $entity->setFoto($filename);

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('alumno_show', array('id' => $entity->getId())));
        }

        return $this->render('JotaMillerBalooBundle:Alumno:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new Alumno entity.
     *
     */
    public function newAction()
    {
        $entity = new Alumno();
        $form   = $this->createForm(new AlumnoType(), $entity);

        return $this->render('JotaMillerBalooBundle:Alumno:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Alumno entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity             = $em->getRepository('JotaMillerBalooBundle:Alumno')->find($id);
        $comentarios        = $em->getRepository('JotaMillerBalooBundle:Comentario')->findBy( array( 'alumno' => $id ), array( 'fechaRegistro' => 'DESC' ));
        $fecha_nacimiento   = $entity->getFechaNacimiento()->format('Y-m-d');
        $edad               = $this->CalculaEdad($fecha_nacimiento);
        $documentos         = $em->getRepository('JotaMillerBalooBundle:Documento')->findAllByEdad($edad);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Alumno entity.');
        }
        $deleteForm = $this->createDeleteForm($id);
        return $this->render('JotaMillerBalooBundle:Alumno:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),   
            'edad'        => $edad,   
            'comentarios' => $comentarios,  
            'documentos'  => $documentos,
            )
        );
    }

    /**
     * Displays a form to edit an existing Alumno entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JotaMillerBalooBundle:Alumno')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Alumno entity.');
        }

        $editForm = $this->createForm(new AlumnoType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JotaMillerBalooBundle:Alumno:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Alumno entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JotaMillerBalooBundle:Alumno')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Alumno entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new AlumnoType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            
            $ext = $editForm['foto']->getData()->guessExtension();
            $filename = sha1(uniqid(mt_rand(), true)) .'.'. $ext;
            $editForm['foto']->getData()->move('uploads/imagenes', $filename);

            $entity->setFoto($filename);
            
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('alumno_edit', array('id' => $id)));
        }

        return $this->render('JotaMillerBalooBundle:Alumno:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Alumno entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('JotaMillerBalooBundle:Alumno')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Alumno entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('alumno'));
    }

    /**
     * Pequeña funcion para el calculo de edad
     * YYYY-mm-dd
     */
    public function CalculaEdad( $fecha ) {
        list($Y,$m,$d) = explode("-",$fecha);
        return( date("md") < $m.$d ? date("Y")-$Y-1 : date("Y")-$Y );
    }

    /**
     * Creates a form to delete a Alumno entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
