<?php

namespace JotaMiller\BalooBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use JotaMiller\BalooBundle\Entity\Apoderado;
use JotaMiller\BalooBundle\Form\ApoderadoType;

/**
 * Apoderado controller.
 *
 */
class ApoderadoController extends Controller
{
    /**
     * Lists all Apoderado entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('JotaMillerBalooBundle:Apoderado')->findAll();

        return $this->render('JotaMillerBalooBundle:Apoderado:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Creates a new Apoderado entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new Apoderado();
        $form = $this->createForm(new ApoderadoType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $hoy    =   ( date('Y-m-d H:i:s') );
            $em = $this->getDoctrine()->getManager();
            $entity->setFechaRegistro( new \DateTime($hoy) );
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('apoderado_show', array('id' => $entity->getId())));
        }

        return $this->render('JotaMillerBalooBundle:Apoderado:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new Apoderado entity.
     *
     */
    public function newAction()
    {
        $entity = new Apoderado();
        $form   = $this->createForm(new ApoderadoType(), $entity);

        return $this->render('JotaMillerBalooBundle:Apoderado:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Apoderado entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JotaMillerBalooBundle:Apoderado')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Apoderado entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JotaMillerBalooBundle:Apoderado:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Apoderado entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JotaMillerBalooBundle:Apoderado')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Apoderado entity.');
        }

        $editForm = $this->createForm(new ApoderadoType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JotaMillerBalooBundle:Apoderado:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Apoderado entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JotaMillerBalooBundle:Apoderado')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Apoderado entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new ApoderadoType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('apoderado_edit', array('id' => $id)));
        }

        return $this->render('JotaMillerBalooBundle:Apoderado:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Apoderado entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('JotaMillerBalooBundle:Apoderado')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Apoderado entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('apoderado'));
    }

    /**
     * Creates a form to delete a Apoderado entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
