<?php

namespace JotaMiller\BalooBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DocumentoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('descripcion')
            ->add('edad_desde', 'integer')
            ->add('edad_hasta', 'integer')
            ->add('tags')
            ->add('file', 'file', array(

                ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'JotaMiller\BalooBundle\Entity\Documento'
        ));
    }

    public function getName()
    {
        return 'jotamiller_baloobundle_documentotype';
    }
}
