<?php

namespace JotaMiller\BalooBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ApoderadoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('apellido')
            ->add('email')
            ->add('telefono')
            ->add('region')
            ->add('comuna')
            ->add('direccion')
            ->add('notas')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'JotaMiller\BalooBundle\Entity\Apoderado'
        ));
    }

    public function getName()
    {
        return 'jotamiller_baloobundle_apoderadotype';
    }
}
