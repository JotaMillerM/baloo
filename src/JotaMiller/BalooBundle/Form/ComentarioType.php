<?php

namespace JotaMiller\BalooBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ComentarioType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fechaRegistro')
            ->add('comentario')
            ->add('estado')
            ->add('usuario')
            ->add('alumno')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'JotaMiller\BalooBundle\Entity\Comentario'
        ));
    }

    public function getName()
    {
        return 'jotamiller_baloobundle_comentariotype';
    }
}
