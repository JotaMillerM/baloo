<?php

namespace JotaMiller\BalooBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AlumnoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('apellido')
            ->add('fechaNacimiento','birthday', array('widget' => 'single_text'))
            ->add('region')
            ->add('comuna')
            ->add('direccion')
            ->add('colegio')
            ->add('notas')
            ->add('foto', 'file', array(
                'data_class' => null, //'Symfony\Component\HttpFoundation\File\File',
                ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'JotaMiller\BalooBundle\Entity\Alumno'
        ));
    }

    public function getName()
    {
        return 'jotamiller_baloobundle_alumnotype';
    }
}
