<?php

namespace JotaMiller\FormBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Campo
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="JotaMiller\FormBundle\Entity\CampoRepository")
 */
class Campo
{
    /**
     * @ORM\ManyToMany(targetEntity="Categoria", inversedBy="campos")
     **/
    private $categorias;

    /**
     * @ORM\ManyToOne(targetEntity="Tipocampo", inversedBy="campos")
     */
    protected $tipocampo;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="pregunta", type="string", length=255)
     */
    private $pregunta;

    /**
     * @var string
     *
     * @ORM\Column(name="respuesta", type="text")
     */
    private $respuesta;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pregunta
     *
     * @param string $pregunta
     * @return Campo
     */
    public function setPregunta($pregunta)
    {
        $this->pregunta = $pregunta;
    
        return $this;
    }

    /**
     * Get pregunta
     *
     * @return string 
     */
    public function getPregunta()
    {
        return $this->pregunta;
    }

    /**
     * Set respuesta
     *
     * @param string $respuesta
     * @return Campo
     */
    public function setRespuesta($respuesta)
    {
        $this->respuesta = $respuesta;
    
        return $this;
    }

    /**
     * Get respuesta
     *
     * @return string 
     */
    public function getRespuesta()
    {
        return $this->respuesta;
    }

    /**
     * Set tipocampo
     *
     * @param \JotaMiller\FormBundle\Entity\Tipocampo $tipocampo
     * @return Campo
     */
    public function setTipocampo(\JotaMiller\FormBundle\Entity\Tipocampo $tipocampo = null)
    {
        $this->tipocampo = $tipocampo;
    
        return $this;
    }

    /**
     * Get tipocampo
     *
     * @return \JotaMiller\FormBundle\Entity\Tipocampo 
     */
    public function getTipocampo()
    {
        return $this->tipocampo;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->categorias = new ArrayCollection();
    }

    /**
     * toString
     */
    public function __toString()
    {
        return $this->pregunta;
    }
    
    /**
     * Add categorias
     *
     * @param \JotaMiller\FormBundle\Entity\Categoria $categorias
     * @return Campo
     */
    public function addCategoria(\JotaMiller\FormBundle\Entity\Categoria $categorias)
    {
        $this->categorias[] = $categorias;
    
        return $this;
    }

    /**
     * Remove categorias
     *
     * @param \JotaMiller\FormBundle\Entity\Categoria $categorias
     */
    public function removeCategoria(\JotaMiller\FormBundle\Entity\Categoria $categorias)
    {
        $this->categorias->removeElement($categorias);
    }

    /**
     * Get categorias
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategorias()
    {
        return $this->categorias;
    }

    /**
     * Set categorias
     *
     * @param string $categorias
     * @return Campo
     */
    public function setCategorias($categorias)
    {
        $this->categorias = $categorias;
    
        return $this;
    }
}