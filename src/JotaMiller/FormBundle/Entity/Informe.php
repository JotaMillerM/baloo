<?php

namespace JotaMiller\FormBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Informe
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="JotaMiller\FormBundle\Entity\InformeRepository")
 */
class Informe
{
    /**
     * @ORM\ManyToOne(targetEntity="JotaMiller\UsuarioBundle\Entity\Usuario", inversedBy="informes")
     */
    protected $usuario;

     /**
    * @ORM\OneToMany(targetEntity="Categoria", mappedBy="informe")
    */
    protected $categorias;
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_creacion", type="datetime")
     */
    private $fechaCreacion;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Informe
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     * @return Informe
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;
    
        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime 
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->categorias = new ArrayCollection();
    }

    /**
     * toString
     */
    public function __toString()
    {
        return $this->nombre;
    }
    
    /**
     * Add categorias
     *
     * @param \JotaMiller\FormBundle\Entity\Categoria $categorias
     * @return Informe
     */
    public function addCategoria(\JotaMiller\FormBundle\Entity\Categoria $categorias)
    {
        $this->categorias[] = $categorias;
    
        return $this;
    }

    /**
     * Remove categorias
     *
     * @param \JotaMiller\FormBundle\Entity\Categoria $categorias
     */
    public function removeCategoria(\JotaMiller\FormBundle\Entity\Categoria $categorias)
    {
        $this->categorias->removeElement($categorias);
    }

    /**
     * Get categorias
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategorias()
    {
        return $this->categorias;
    }

    /**
     * Set usuario
     *
     * @param \JotaMiller\UsuarioBundle\Entity\Usuario $usuario
     * @return Informe
     */
    public function setUsuario(\JotaMiller\UsuarioBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;
    
        return $this;
    }

    /**
     * Get usuario
     *
     * @return \JotaMiller\UsuarioBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
}