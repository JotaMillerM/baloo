<?php
/**
 * Defie los tipos de campos para los diferentes elementos dentro del 
 * formulario
 * 
 * @author Jimmy Miller < henuxmail@gmail.com >
 * 
 */
namespace JotaMiller\FormBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Tipocampo
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="JotaMiller\FormBundle\Entity\TipocampoRepository")
 */
class Tipocampo
{
    /**
    * @ORM\OneToMany(targetEntity="Campo", mappedBy="tipocampo")
    */
    protected $campos;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text")
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="atributos", type="text")
     */
    private $atributos;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Tipocampo
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Tipocampo
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set atributos
     *
     * @param string $atributos
     * @return Tipocampo
     */
    public function setAtributos($atributos)
    {
        $this->atributos = $atributos;
    
        return $this;
    }

    /**
     * Get atributos
     *
     * @return string 
     */
    public function getAtributos()
    {
        return $this->atributos;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->campos = new ArrayCollection();
    }

    /**
     * 
     */
    public function __toString()
    {
        return $this->nombre;
    }
    
    /**
     * Add campos
     *
     * @param \JotaMiller\FormBundle\Entity\Campo $campos
     * @return Tipocampo
     */
    public function addCampo(\JotaMiller\FormBundle\Entity\Campo $campos)
    {
        $this->campos[] = $campos;
    
        return $this;
    }

    /**
     * Remove campos
     *
     * @param \JotaMiller\FormBundle\Entity\Campo $campos
     */
    public function removeCampo(\JotaMiller\FormBundle\Entity\Campo $campos)
    {
        $this->campos->removeElement($campos);
    }

    /**
     * Get campos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCampos()
    {
        return $this->campos;
    }
}