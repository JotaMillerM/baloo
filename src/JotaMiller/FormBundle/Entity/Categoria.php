<?php

namespace JotaMiller\FormBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Categoria
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="JotaMiller\FormBundle\Entity\CategoriaRepository")
 */
class Categoria
{
    /**
     * @ORM\ManyToOne(targetEntity="Informe", inversedBy="categorias")
     */
    protected $informe;

    /**
     * @ORM\ManyToMany(targetEntity="Campo", mappedBy="categorias")
     **/
    private $campos;
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=200)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text")
     */
    private $descripcion;

    /**
     * @var integer
     *
     * @ORM\Column(name="estado", type="smallint")
     */
    private $estado;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Categoria
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Categoria
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set estado
     *
     * @param integer $estado
     * @return Categoria
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return integer 
     */
    public function getEstado()
    {
        return $this->estado;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->campos = new ArrayCollection();
    }

    /**
     * toString
     */
    public function __toString()
    {
        return $this->nombre;
    }
    
    /**
     * Add campos
     *
     * @param \JotaMiller\FormBundle\Entity\Campo $campos
     * @return Categoria
     */
    public function addCampo(\JotaMiller\FormBundle\Entity\Campo $campos)
    {
        $this->campos[] = $campos;
    
        return $this;
    }

    /**
     * Remove campos
     *
     * @param \JotaMiller\FormBundle\Entity\Campo $campos
     */
    public function removeCampo(\JotaMiller\FormBundle\Entity\Campo $campos)
    {
        $this->campos->removeElement($campos);
    }

    /**
     * Get campos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCampos()
    {
        return $this->campos;
    }

    /**
     * Set informe
     *
     * @param \JotaMiller\FormBundle\Entity\Informe $informe
     * @return Categoria
     */
    public function setInforme(\JotaMiller\FormBundle\Entity\Informe $informe = null)
    {
        $this->informe = $informe;
    
        return $this;
    }

    /**
     * Get informe
     *
     * @return \JotaMiller\FormBundle\Entity\Informe 
     */
    public function getInforme()
    {
        return $this->informe;
    }
}