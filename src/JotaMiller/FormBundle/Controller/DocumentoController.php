<?php

namespace JotaMiller\FormBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use JotaMillerFormBundle\FormBundle\Entity\Tipocampo;
use JotaMillerFormBundle\FormBundle\Entity\Campo;
use JotaMillerFormBundle\FormBundle\Entity\Categoria;
use JotaMillerFormBundle\FormBundle\Entity\Informe;

/**
 * Documento controller.
 *
 */
class DocumentoController extends Controller
{
	
	public function indexAction()
	{
		$em = $this->getDoctrine()->getManager();

        $informes = $em->getRepository('JotaMillerFormBundle:Informe')->findAll();
        $format = "pdf";

		return $this->render('JotaMillerFormBundle:Documento:index.html.twig', array(
			'informes' => $informes,
  		));

        return $this->render('JotaMillerFormBundle:Documento:index.pdf.twig', array(
			'informes' => $informes,
    	));

	}

	/**
	 * Crea un nuevo documento
	 */
	public function newAction()
	{

        return $this->render('JotaMillerFormBundle:Documento:new.html.twig', array(

        ));

	}
}