<?php

namespace JotaMiller\FormBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use JotaMiller\FormBundle\Entity\Tipocampo;
use JotaMiller\FormBundle\Form\TipocampoType;

/**
 * Tipocampo controller.
 *
 */
class TipocampoController extends Controller
{
    /**
     * Lists all Tipocampo entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('JotaMillerFormBundle:Tipocampo')->findAll();

        return $this->render('JotaMillerFormBundle:Tipocampo:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Creates a new Tipocampo entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new Tipocampo();
        $form = $this->createForm(new TipocampoType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('tipocampo_show', array('id' => $entity->getId())));
        }

        return $this->render('JotaMillerFormBundle:Tipocampo:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new Tipocampo entity.
     *
     */
    public function newAction()
    {
        $entity = new Tipocampo();
        $form   = $this->createForm(new TipocampoType(), $entity);

        return $this->render('JotaMillerFormBundle:Tipocampo:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Tipocampo entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JotaMillerFormBundle:Tipocampo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tipocampo entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JotaMillerFormBundle:Tipocampo:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Tipocampo entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JotaMillerFormBundle:Tipocampo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tipocampo entity.');
        }

        $editForm = $this->createForm(new TipocampoType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JotaMillerFormBundle:Tipocampo:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Tipocampo entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JotaMillerFormBundle:Tipocampo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tipocampo entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new TipocampoType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('tipocampo_edit', array('id' => $id)));
        }

        return $this->render('JotaMillerFormBundle:Tipocampo:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Tipocampo entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('JotaMillerFormBundle:Tipocampo')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Tipocampo entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('tipocampo'));
    }

    /**
     * Creates a form to delete a Tipocampo entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
