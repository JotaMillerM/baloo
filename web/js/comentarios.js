// Archivos para controlar los comentarios

$(document).ready(function() {

	$.timeago.settings.strings = {
	   prefixAgo: "hace",
	   prefixFromNow: "dentro de",
	   suffixAgo: "",
	   suffixFromNow: "",
	   seconds: "menos de un minuto",
	   minute: "un minuto",
	   minutes: "unos %d minutos",
	   hour: "una hora",
	   hours: "%d horas",
	   day: "un día",
	   days: "%d días",
	   month: "un mes",
	   months: "%d meses",
	   year: "un año",
	   years: "%d años"
	};
	$(".timeago").timeago();

	$("#message-form").submit(function(e) {
		e.preventDefault();

		 var url = $('#url_comentario_add').val(); // the script where you handle the form input.
		 var txt_comentario = $('#texto_comentario').val();
		 var id_alumno = $('#id_alumno').val();
		$.ajax({
           	type: "POST",
           	url: url,
           	cache: false,
           	data: {
           		comentario: txt_comentario,
           		alumno: id_alumno
           	}, 
           	success: function(data)
           	{
           		
           		$('#lista_historial').prepend('<li class="left"><div class="image"><i class="icon-comments-alt"></i></div><div class="message"><span class="caret"></span><span class="name">'+data['autor']+'</span><p>'+data['comentario']+'</p><span class="time timeago" title="'+data['fecha']['date']+'"></span></div></li>');
           		$('#texto_comentario').val("");
           		console.log(data['fecha']);
           		$(".timeago").timeago();
           	}
        });
	});
});